﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baloon : MonoBehaviour {
    
    int     _breath;
    float   _time;
    bool    _start;

    void Start () {
        _breath = 100;
        _time = 0;
        _start = false;
    }

    void Update () {

        if (transform.localScale.x < 10 && transform.localScale.x > 0)
        {
            if (Input.GetKeyDown(KeyCode.Space) && _breath > 0)
            {
                transform.localScale += new Vector3(0.5F, 0.5F, 0);
                _breath = (_breath <= 25 ? 0 : _breath - 25);
                _start = true;
            }
            else if (_start)
            {
                transform.localScale -= new Vector3(0.05F, 0.05F, 0);
                _breath = (_breath >= 99 ? 100 : _breath + 1);
            }
        }
        else
        {
            Destroy(this.gameObject);
            Debug.Log("Balloon life time:" + Mathf.RoundToInt(_time) + "s");
        }
        _time += Time.deltaTime;
    }
}
