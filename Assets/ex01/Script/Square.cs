﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour {

    float _speed;

    void Start () {
        _speed = Random.Range(2F, 10F);
    }

    void Update () {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
    }
}
