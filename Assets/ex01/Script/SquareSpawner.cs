﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareSpawner : MonoBehaviour {

    public GameObject[]     squares;
    public GameObject[]     squaresRef;
    UnityEngine.KeyCode[]   relativeKeys;

    float   _spawnTimer;
    double  _precision;

    void Start () {
        squaresRef = new GameObject[3];
        relativeKeys = new UnityEngine.KeyCode[3];
        relativeKeys[0] = KeyCode.A;
        relativeKeys[1] = KeyCode.S;
        relativeKeys[2] = KeyCode.D;
        _spawnTimer = Random.Range(0.25F, 1F);
    }

    void Update () {

        if (_spawnTimer < 0)
            SpawnSquare(Random.Range(0, 3));
        else
            _spawnTimer -= Time.deltaTime;
        CheckSquare();
    }

    void SpawnSquare(int squareToSpawn) {

        if (!squaresRef[squareToSpawn])
        {
            squaresRef[squareToSpawn] = Instantiate(squares[squareToSpawn]);
            _spawnTimer = Random.Range(0.25F, 1F);
        }
    }

    void CheckSquare() {
        for (int i = 0; i < 3; i++)
        {
            if (squaresRef[i] && (Input.GetKeyDown(relativeKeys[i]) ||
                     squaresRef[i].transform.position.y < -3.75))
            {
                _precision = (squaresRef[i].transform.position.y - (-3)) * 10;
                Debug.Log("Precision:" + System.Math.Round(_precision, 2));
                Destroy(squaresRef[i]);
            }
        }
    }
}
